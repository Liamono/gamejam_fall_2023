using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource source;

    //public AudioClip combatClip;

    public GameState stateToPlayDuring;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        source.enabled = GameManager.instance.gameState == stateToPlayDuring;
        //if (GameManager.instance.gameState == stateToPlayDuring)
        //{
        //    source.enabled = true;
        //}
        //else
        //{
        //    source.enabled = false;

        //}
    }
}
