using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectScript : MonoBehaviour
{
    public float duration = 4f;
    public AudioSource audioSource;
    public float minPitch = 1f;
    public float maxPitch = 1f;

    // Start is called before the first frame update
    void Start()
    {
        float RandomPitch = Random.Range(minPitch, maxPitch);
        audioSource.pitch = RandomPitch;

        Destroy(this, duration);
    }
}
