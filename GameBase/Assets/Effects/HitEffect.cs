using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffect : MonoBehaviour
{
    public float duration = .15f;
    public GameObject secondaryPrefabToInstantiate;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Instantiate(secondaryPrefabToInstantiate, transform.position, Quaternion.identity);
        Destroy(gameObject, duration);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
