using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackScript : StateMachineBehaviour
{
    EnemyController enemy;

    public float hitDistance = 1f;
    public float hitTime = .5f;
    public bool hasAppliedDamage = false;
     //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        hasAppliedDamage = false;
        enemy = animator.GetComponent<EnemyController>();

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime > hitTime)
        {
            if (hasAppliedDamage == false)
            {
                hasAppliedDamage = true;
                float distance = Vector3.Distance(enemy.gameObject.transform.position, enemy.player.transform.position);
                if (distance < hitDistance)
                {
                    //PlayerController player = enemy.player.GetComponent<PlayerController>();
                    //player.anim.applyRootMotion = false;
                    enemy.player.GetComponent<PlayerController>().TakeDamage(enemy, 1f);
                    Vector3 theVector = (enemy.WeaponReference.transform.position + enemy.player.GetComponent<PlayerController>().ragdollManager.testRB.transform.position) / 2;
                    GameObject.Instantiate(enemy.hitEffectPrefab, theVector, Quaternion.identity);
                    //enemy.player.GetComponent<PlayerController>().LookAtSpecial(enemy.gameObject);
                }
            }

        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.attackIndicator.SetActive(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
