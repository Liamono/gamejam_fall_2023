using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    private GameObject symbol;
    private GameObject particle;
    private GameObject canvas;
    private GameManager manager;

    public Image theImage;
    public ParticleSystem particles;
    public float fadeTime = 1f;
    private bool fadedIn = false;

    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        canvas = transform.GetChild(0).gameObject;

        canvas.GetComponent<Canvas>().worldCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        particles.startColor = Color.clear;
        theImage.color = Color.clear;

        FadeIn(true);
    }

    void FixedUpdate()
    {
        //REMINDER: Fix lerp factor so that this happens more fixed
    }

    public void FadeIn(bool direction)
    {
        if (direction)
        {
            StartCoroutine(LerpImage(theImage.color, Color.red, fadeTime));
            StartCoroutine(LerpParticles(particles.startColor, Color.red, fadeTime));
        }
        else
        {
            StartCoroutine(LerpImage(theImage.color, Color.clear, fadeTime));
            StartCoroutine(LerpParticles(particles.startColor, Color.clear, fadeTime));
        }
    }

    public void SpawnEnemy()
    {
        fadedIn = true;

        GameObject newEnemy = enemy;
        
        manager.AddEnemy(Instantiate(newEnemy, new Vector3(transform.position.x, 2f, transform.position.z), Quaternion.identity));

        StartCoroutine(SpawnAnimationBuffer());
    }

    IEnumerator SpawnAnimationBuffer()
    {
        yield return new WaitForSeconds(3);
        FadeIn(false);
    }

    IEnumerator LerpImage(Color start, Color end, float t)
    {
        if(fadedIn)
            Debug.Log("Starting Spawner...");

        float elapsed_time = 0;

        while (elapsed_time < t) //Inside the loop until the time expires
        {
            Color newColor = new Color
        (
            start.r + (end.r - start.r) * (elapsed_time / t),
            start.g + (end.g - start.g) * (elapsed_time / t),
            start.b + (end.b - start.b) * (elapsed_time / t),
            start.a + (end.a - start.a) * (elapsed_time / t)
        );

            theImage.color = newColor;

            yield return null; //Waits/skips one frame

            elapsed_time += Time.deltaTime; //Adds to the elapsed time the amount of time needed to skip/wait one frame
        }

        if (!fadedIn)
        {
            Debug.Log("SPAWNING ENEMY...");
            SpawnEnemy();
        }
        else
            Destroy(gameObject);
    }
    IEnumerator LerpParticles(Color start, Color end, float t)
    {
        float elapsed_time = 0;

        while (elapsed_time < t)
        {
            Color newColor = new Color
        (
            start.r + (end.r - start.r) * (elapsed_time / t),
            start.g + (end.g - start.g) * (elapsed_time / t),
            start.b + (end.b - start.b) * (elapsed_time / t),
            start.a + (end.a - start.a) * (elapsed_time / t)
        );

            particles.startColor = newColor;

            yield return null;

            elapsed_time += Time.deltaTime;
        }
    }
}
