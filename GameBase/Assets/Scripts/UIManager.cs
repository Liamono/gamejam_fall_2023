using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    bool startButton = false;
    bool startButtonRelease;
    bool paused = false;
    bool firstPress = true;

    bool leftBump = false;
    bool leftReleased = false;
    bool wentLeft;

    bool rightBump = false;
    bool rightReleased = false;
    bool wentRight;

    private Vector3 middlePos = new Vector3(0f,0f,0f);
    private Vector3 rightPos = new Vector3(1920f, 0f, 0f);
    private Vector3 leftPos = new Vector3(-1920f, 0f, 0f);

    public float menuSpeed = 1f;

    [SerializeField]
    Screen currentScreen;

    [SerializeField]
    GameObject mainScreen;
    [SerializeField]
    GameObject leftScreen;
    [SerializeField]
    GameObject rightScreen;

    enum Screen
    {
        main,
        left,
        right
    }

    GameManager manager;

    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject postMenu;
    [SerializeField]
    private GameObject gameMenu;

    [SerializeField]
    GameObject scoreboardParent;

    [System.Serializable]
    class Result
    {
        public string playerName;
        public int enemiesKilled;
        public float timeLasted;
    }

    [SerializeField]
    private List<Result> scoreboard = new List<Result>();
    private string PlayerPrefsBaseKey = "leaderboard";
    [SerializeField]
    private int scoreboardCount = 10;

    public GameObject leaderboardEntryObject;
    private List<GameObject> scoreboardEntries = new List<GameObject>();

    [SerializeField]
    private GameObject inputField;
    [SerializeField]
    private GameObject enemiesKilledText;
    [SerializeField]
    private GameObject timeLastedText;

    void Start()
    {
        manager = GetComponent<GameManager>();
        currentScreen = Screen.main;

        LoadScoreboard();
    }
    void Update()
    {
        if (manager.gameState == GameState.Pregame)
        {
            MenuScrollCheck();
            if (startButton && currentScreen == Screen.main)
            {
                firstPress = true;
                mainMenu.SetActive(false);
                manager.StartGame();
            }
        }
        else if (manager.gameState == GameState.Game)
        {
            gameMenu.SetActive(true);
            PauseToggleButtonCheck();

            if (paused)
                Time.timeScale = 0f;
            else
                Time.timeScale = 1f;
        }
        else if (manager.gameState == GameState.PostGame)
        {
            postMenu.SetActive(true);
            gameMenu.SetActive(false);
            inputField.GetComponent<TMP_InputField>().Select();

            if (startButton)
            {
                manager.ConfirmScore(inputField.GetComponent<TMP_InputField>().text);
            }
        }
    }

    public void SetEndScreenStats(int killCount, float time)
    {
        enemiesKilledText.GetComponent<TextMeshProUGUI>().text = "Enemies Killed: " + killCount.ToString();
        timeLastedText.GetComponent<TextMeshProUGUI>().text = "Time Survived: " + time.ToString();
    }

    void PauseToggleButtonCheck()
    {
        if (startButton && !paused && !firstPress)
        {
            pauseMenu.SetActive(true);
        }
        else if (startButton && paused)
        {
            pauseMenu.SetActive(false);
        }

        if (startButtonRelease && firstPress)
        {
            firstPress = false;
            startButtonRelease = false;
        }
        else if (startButtonRelease && !paused)
        {
            paused = true;
            startButtonRelease = false;
        }
        else if (startButtonRelease && paused)
        {
            paused = false;
            startButtonRelease = false;
        }
    }

    void MenuScrollCheck()
    {
        if (leftBump && !wentLeft)
        {
            wentLeft = true;

            switch (currentScreen)
            {
                case Screen.main: //Move left screen rightwards
                    currentScreen = Screen.left;
                    
                    mainScreen.SetActive(false);
                    //leftScreen.transform.localPosition = leftPos;
                    StartCoroutine(MoveLeftScreen(leftPos, middlePos, menuSpeed));
                    break;
                case Screen.left:
                    //Do nothing
                    break;
                case Screen.right: //Move right screen rightwards
                    currentScreen = Screen.main;
                    
                    mainScreen.SetActive(true);
                    //rightScreen.transform.localPosition = middlePos;
                    StartCoroutine(MoveRightScreen(middlePos, rightPos, menuSpeed));
                    break;
            }
        }

        if (rightBump && !wentRight)
        {
            wentRight = true;

            switch (currentScreen)
            {
                case Screen.main: //Move Right screen leftwards
                    currentScreen = Screen.right;
                    
                    mainScreen.SetActive(false);
                    //rightScreen.transform.localPosition = rightPos;
                    StartCoroutine(MoveRightScreen(rightPos, middlePos, menuSpeed));
                    break;
                case Screen.left: //Move Left screen leftwards
                    currentScreen = Screen.main;
                    
                    mainScreen.SetActive(true);
                    //leftScreen.transform.localPosition = middlePos;
                    StartCoroutine(MoveLeftScreen(middlePos, leftPos, menuSpeed));
                    break;
                case Screen.right:
                    //Do nothing
                    break;
            }
        }

        if (leftReleased)
        {
            wentLeft = false;
            leftReleased = false;
        }

        if (rightReleased)
        {
            wentRight = false;
            rightReleased = false;
        }
    }

    IEnumerator MoveRightScreen(Vector3 start, Vector3 end, float t)
    {
        float elapsed_time = 0;

        while (elapsed_time < t)
        {
            Vector3 newPos = start + (end - start) * (elapsed_time / t);

            rightScreen.transform.localPosition = newPos;

            yield return null;

            elapsed_time += Time.deltaTime;
        }
        rightScreen.transform.localPosition = end;
    }

    IEnumerator MoveLeftScreen(Vector3 start, Vector3 end, float t)
    {
        float elapsed_time = 0;

        while (elapsed_time < t)
        {
            Vector3 newPos = start + (end - start) * (elapsed_time / t);

            leftScreen.transform.localPosition = newPos;

            yield return null;

            elapsed_time += Time.deltaTime;
        }
        leftScreen.transform.localPosition = end;
    }

    private void SortScores()
    {
        scoreboard.Sort((a, b) => b.enemiesKilled.CompareTo(a.enemiesKilled));
    }

    public void SaveScoreToScoreboard()
    {
        SortScores();
        for (int i = 0; i < scoreboardCount; ++i)
        {
            Result aResult = scoreboard[i];
            PlayerPrefs.SetString(PlayerPrefsBaseKey +  i + ".playerName", aResult.playerName);
            PlayerPrefs.SetInt(PlayerPrefsBaseKey +  i + ".enemiesKilled", aResult.enemiesKilled);
            PlayerPrefs.SetFloat(PlayerPrefsBaseKey +  i + ".timeLasted", aResult.timeLasted);
        }
        Debug.Log("Saved Scores!");
    }

    public void LoadScoreboard()
    {
        scoreboard.Clear();

        foreach (GameObject g in scoreboardEntries)
        {
            Destroy(g);
            scoreboardEntries.Remove(g);
        }

        for (int i = 0; i < scoreboardCount; ++i)
        {
            Result aResult = new Result();
            aResult.playerName = PlayerPrefs.GetString(PlayerPrefsBaseKey + i + ".playerName", "");
            aResult.enemiesKilled = PlayerPrefs.GetInt(PlayerPrefsBaseKey + i + ".enemiesKilled", -1);
            aResult.timeLasted = PlayerPrefs.GetFloat(PlayerPrefsBaseKey + i + ".timeLasted", -1);
            scoreboard.Add(aResult);
        }

        SortScores();

        foreach (Result r in scoreboard)
        {
            GameObject newEntry = Instantiate(leaderboardEntryObject, scoreboardParent.transform);
            scoreboardEntries.Add(newEntry);
            newEntry.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = r.playerName;
            newEntry.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = r.enemiesKilled.ToString();
            newEntry.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = r.timeLasted.ToString();
        }
    }

    public void AddNewScore(string aName, int aEnemyScore, float aTime)
    {
        enemiesKilledText.GetComponent<TextMeshProUGUI>().text = aEnemyScore.ToString();
        timeLastedText.GetComponent<TextMeshProUGUI>().text = aTime.ToString();

        Result newResult = new Result();

        newResult.playerName = aName;
        newResult.enemiesKilled = aEnemyScore;
        newResult.timeLasted = aTime;

        foreach (Result r in scoreboard)
        {
            if (r.playerName == newResult.playerName && newResult.enemiesKilled > r.enemiesKilled) //If name matches AND score is higher then replace their score
                scoreboard.Remove(r);
            else if (r.playerName == newResult.playerName) //Do NOT add new score if name matches but score is lower
                return;
        }
        scoreboard.Add(newResult);

        SaveScoreToScoreboard();
    }

    public void StartButton(InputAction.CallbackContext context)
    {
        startButton = context.performed;
        startButtonRelease = context.canceled;
    }

    public void LeftBumper(InputAction.CallbackContext context)
    {
        leftBump = context.performed;
        leftReleased = context.canceled;
    }
    public void RightBumper(InputAction.CallbackContext context)
    {
        rightBump = context.performed;
        rightReleased = context.canceled;
    }
}