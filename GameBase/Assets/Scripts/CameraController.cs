using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private GameManager manager;

    public float combatLerpFactorM = 3f;
    public float combatLerpFactorL = 8f;

    public GameObject combatRigTarget;
    public GameObject lookLerpTarget;
    public GameObject player;

    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        lookLerpTarget.transform.parent = null;
        lookLerpTarget.transform.position = player.transform.position;
    }

    void FixedUpdate()
    {
        if (manager.gameState == GameState.Pregame)
            RotateCamera();
    }

    private void Update()
    {
        if (manager.gameState == GameState.Game)
        {
            CombatMode();
        }
    }

    void RotateCamera() //Rotates camera around the middle of the scene
    {
        transform.LookAt(new Vector3(0f, 2f, 0f));
        transform.Translate(Vector3.right * Time.fixedDeltaTime);
    }

    void CombatMode()
    {
        transform.position = Vector3.Lerp(transform.position, combatRigTarget.transform.position, Time.deltaTime * combatLerpFactorM);
        //transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, combatRigTarget.transform.rotation, Time.deltaTime * combatLerpFactorL);
        lookLerpTarget.transform.position = Vector3.Lerp(lookLerpTarget.transform.position, player.transform.position, Time.deltaTime * combatLerpFactorL);
        transform.LookAt(lookLerpTarget.transform.position);
    }
}
