using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
    public GameManager gameManager;

    //private float health;
    private float speed;
    private float rotationSpeed;

    public PlayerInputScript inputScript;
    public Animator anim;
    public EnemDetector enemyDetector;
    public PlayerControllerAttack attackScript;
    public RagdollManager ragdollManager;

    public float lerpFactor = 1f;

    public PlayerState playerState;
    public enum PlayerState
    {
        normal,
        attackLerping,
        attacking,
        flinching,
        dead,
    }

    //public bool recievingInput = true;
    public bool respondingToLeftStick = true;
    public bool canAttack = true;

    public Vector3 startHeight;

    public GameObject hitEffectPrefab;
    public GameObject WeaponReference;
    public ThuribleScript thuribleScript;

    private void Awake()
    {
        startHeight = gameObject.transform.position;
    }

    //public EnemyController currentTarget;

    void Start()
    {
        
    }

    void Update()
    {
        if (gameManager.gameState == GameState.Game)
        {
            if (respondingToLeftStick)
            {
                FaceControlStick();
                SetMoveFactor();
            }
            MonitorAttackButton();
        }

    }

    void MonitorAttackButton()
    {
        if (canAttack && inputScript.xButton == true && enemyDetector.currentTarget != null)
        {
            canAttack = false;
            respondingToLeftStick = false;
            attackScript.Attack(enemyDetector.currentTarget);
        }
    }
    void FaceControlStick()
    {

        if (inputScript.leftStickInput.magnitude > .1f)
        {
            float stickAngle = Mathf.Atan2(inputScript.leftStickInput.x, inputScript.leftStickInput.y);
            stickAngle = stickAngle * Mathf.Rad2Deg;
            Quaternion targetAngle = Quaternion.Euler(0, stickAngle, 0);

            Vector3 lol = transform.position - Camera.main.transform.position;
            Vector3 flattenedLol = lol;
            flattenedLol.y = 0;

            Vector3 controlLol = new Vector3(inputScript.leftStickInput.x, 0, inputScript.leftStickInput.y);

            Vector3 newLol = flattenedLol;
            newLol = Quaternion.AngleAxis(stickAngle, Vector3.up) * newLol;
            newLol = newLol.normalized;
            float UltraAngle = Mathf.Atan2(newLol.x, newLol.z);
            UltraAngle = UltraAngle * Mathf.Rad2Deg;
            Quaternion newTargetAngle = Quaternion.Euler(0, UltraAngle, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, newTargetAngle, lerpFactor * Time.deltaTime);

        }

    }
    void SetMoveFactor()
    {
        anim.SetFloat("MoveFactor", inputScript.leftStickInput.magnitude);
    }

    //public void LookAtSpecial(GameObject target)
    //{
    //    transform.LookAt(target.transform);
    //    Vector3 lockableVect = transform.rotation.eulerAngles;
    //    lockableVect.z = 0;
    //    lockableVect.x = 0;
    //    transform.rotation = Quaternion.Euler(lockableVect);
    //}

    public void Death()
    {
        Debug.Log($"PLAYERDEAD");
        playerState = PlayerState.dead;
        //anim.CrossFade("Death", .25f);
        gameManager.PlayerDied();
    }

    public override void TakeDamage(Character dealer, float damage)
    {
        base.TakeDamage(dealer, damage);
        canAttack = false;
        respondingToLeftStick = false;
        if (playerState != PlayerState.dead)
        {
            anim.applyRootMotion = false;
            LookAtSpecial(dealer.gameObject);
            //transform.LookAt(dealer.transform);

            //anim.applyRootMotion = true;
            //transform.Rotate(Vector3.up);
            Debug.Log($"dealer: {dealer.gameObject.name}");


            if (health > 0)
            {
                anim.CrossFade("Flinch", .25f);
            }
            else
            {
                ragdollManager.ActivateRagdoll(dealer.gameObject.transform.position);
                Death();

            }
        }

    }
    //public void GetHit(EnemyController dealer)
}
