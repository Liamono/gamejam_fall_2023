using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    public Animator anim;

    public GameObject root;

    public Rigidbody testRB;

    public float forceMultiplier = 10f;

    public GameObject testObject;

    public List<Rigidbody> rigidBodiesToReset = new List<Rigidbody>();

    public bool testButton;
    public bool testButton2;

    public Vector3 forwardVect;

    public bool Alt = false;
    public Vector3 hipsOriginalPosition;
    public GameObject spineObjectToReset;


    // Start is called before the first frame update
    void Awake()
    {
        //DeactivateRagdoll();
        foreach (Rigidbody rb in rigidBodiesToReset)
        {
            rb.velocity = Vector3.zero;

            DisableRigidBodyPhysics(rb);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (testButton)
        {
            testButton = false;
            ActivateRagdoll(testObject.transform.position);
        }
        if (testButton2)
        {
            testButton2 = false;
            DeactivateRagdoll();
        }

        forwardVect = root.transform.forward;
    }

    public void DisableRigidBodyPhysics(Rigidbody rb)
    {
        rb.isKinematic = true;
        Collider collider = rb.gameObject.GetComponent<Collider>();
        if (collider != null)
        {
            collider.enabled = false;
        }
    }
    public void EnableRigidbodyPhysics(Rigidbody rb)
    {
        rb.isKinematic = false;
        Collider collider = rb.gameObject.GetComponent<Collider>();
        if (collider != null)
        {
            collider.enabled = true;
        }
    }

    public void ActivateRagdoll(Vector3 impactLocation)
    {
        //root.transform.

        anim.enabled = false;

        foreach (Rigidbody rb in rigidBodiesToReset)
        {
            EnableRigidbodyPhysics(rb);
        }

        Vector3 direction = testRB.transform.position - impactLocation;
        direction = direction.normalized;
        testRB.velocity = direction * forceMultiplier;
    }

    public void DeactivateRagdoll()
    {
        foreach (Rigidbody rb in rigidBodiesToReset)
        {
            rb.velocity = Vector3.zero;

            DisableRigidBodyPhysics(rb);
        }

        Vector3 rootPosition = root.transform.position;
        rootPosition.y = 0;
        transform.position = rootPosition;

        if (Alt)
        {
            root.transform.localPosition = hipsOriginalPosition;
            spineObjectToReset.transform.localRotation = Quaternion.identity;
        }
        else
        {
            root.transform.position = rootPosition;

        }
        anim.enabled = true;
        //anim.enabled = false;
        //anim.enabled = true;

        if (root.transform.forward.y > 0)
        {
            //get up off back
            anim.Play("GetUpBack");
        }
        else
        {
            //get up off front
            anim.Play("GetUpFront");
        }
        //anim.CrossFade()
        //Vector3 rootPosition = root.transform.position;

    }
}
