using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : Character
{
    //public int health = 3;

    public EnemyState enemyState = EnemyState.spawning;
    public Animator anim;
    public enum EnemyState
    {
        spawning,
        stalking,
        approaching,
        attacking,
        knockedDown,
        knockedOut,
    }
    public GameObject player;
    public GameObject attackIndicator;
    public GameObject dizzyIndicator;
    public GameObject hitEffectPrefab;
    public GameObject WeaponReference;


    public RagdollManager ragdollManager;
    public float knockoutTimer;
    public float knockoutDelay = 2f;

    public float moveX = 0f;
    public float moveY = 0f;
    public float lerpX = 0f;
    public float lerpY = 0f;
    public float lerpFactor = 5f;
    public float randomX;
    public float randomY;

    public float stalkDistanceMin = 3f;
    public float stalkDistanceMax = 6f;
    public float randomTimer;
    public float timerMin = 1f;
    public float timerMax = 3f;

    //public float spawnInTimer;
    //public float spawnInDuration;

    public float strikeDistance = 1f;
    public List<string> attackStateNames = new List<string>();

    public float deathTimer;
    public float deathDelay = 3f;



    public bool testButton;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //spawnInTimer = Time.time + spawnInDuration;
    }

    void Update()
    {
        //if (enemyState == EnemyState.spawning)
        //{

        //}

        if (enemyState == EnemyState.stalking || enemyState == EnemyState.approaching)
        {
            FaceThePlayer();
        }
        if (enemyState == EnemyState.stalking)
        {
            stalkUpdate();
        }
        if (enemyState == EnemyState.approaching)
        {
            ApproachFunct();
        }

        if (enemyState == EnemyState.knockedDown)
        {
            KOUpdate();
        }
        if (enemyState == EnemyState.knockedOut)
        {
            DeathUpdate();
        }


        TrackRandomFloats();
        SyncAnimator();

        if (testButton)
        {
            testButton = false;
            Engage();
        }
        setAttackIndicator();
    }

    void SpawnUpdateFunct()
    {
        //if (spawnInTimer < Time.deltaTime)
       // {

        //}
    }

    public void Spawn()
    {
        
    }

    public void Die()
    {
        //enem
    }
    void SyncAnimator()
    {
        lerpX = Mathf.Lerp(lerpX, moveX, Time.deltaTime * lerpFactor);
        lerpY = Mathf.Lerp(lerpY, moveY, Time.deltaTime * lerpFactor);


        anim.SetFloat("moveX", lerpX);
        anim.SetFloat("moveY", lerpY);
    }
    public void FaceThePlayer()
    {
        LookAtSpecial(player);
    }

    public void stalkUpdate()
    {
        float currentDistance = Vector3.Distance(transform.position, player.transform.position);

        if (currentDistance < stalkDistanceMin)
        {
            moveY = -1f;
        }
        else if (currentDistance > stalkDistanceMax)
        {
            moveY = 1f;
        }
        else
        {
            moveY = randomY;
            moveX = randomX;

        }
    }

    public void Engage()
    {
        enemyState = EnemyState.approaching;

    }

    public void ApproachFunct()
    {
        float currentDistance = Vector3.Distance(transform.position, player.transform.position);
        if (currentDistance < strikeDistance)
        {
            Attack();
        }
        else
        {
            moveY = 1f;
        }
    }

    public void Attack()
    {
        enemyState = EnemyState.stalking;
        int randomIndex = Random.Range(0, attackStateNames.Count);
        anim.CrossFade(attackStateNames[randomIndex], .25f);
    }
    void AttackFunct()
    {

    }

    public void TrackRandomFloats()
    {
        if (Time.time > randomTimer)
        {
            int coinflip = Random.Range(0, 4);
            randomTimer = Time.time + Random.Range(timerMin, timerMax);

            if (coinflip != 0)
            {
                randomX = Random.Range(-1f, 1f);
                randomY = Random.Range(-1f, 1f);
                Debug.Log("yesMove");

            }
            else
            {
                Debug.Log("noMove");
                randomX = 0;
                randomY = 0;
            }

        }
    }
    void setAttackIndicator()
    {
        if (enemyState == EnemyState.approaching || enemyState == EnemyState.attacking)
        {
            attackIndicator.SetActive(true);
        }
        if (enemyState == EnemyState.knockedDown || enemyState == EnemyState.knockedOut)
        {
            attackIndicator.SetActive(false);

        }

        dizzyIndicator.SetActive(enemyState == EnemyState.knockedDown);
    }

    public override void TakeDamage(Character dealer, float damage)
    {
        base.TakeDamage(dealer, damage);
        ragdollManager.ActivateRagdoll(dealer.gameObject.transform.position);

        if (health > 0)
        {
            knockoutTimer = Time.time + knockoutDelay;
            enemyState = EnemyState.knockedDown;
        }
        else
        {
            enemyState = EnemyState.knockedOut;
            deathTimer = Time.time + deathDelay;
        }


    }

    public void KOUpdate()
    {
        if (Time.time > knockoutTimer)
        {
            //get up
            ragdollManager.DeactivateRagdoll();
            enemyState = EnemyState.stalking;
            
        }
    }

    void DeathUpdate()
    {
        if (Time.time > deathTimer)
        {
            PlayerController playe = player.GetComponent<PlayerController>();
            if (playe.enemyDetector.enemiesToAttack.Contains(this))
            {
                playe.enemyDetector.enemiesToAttack.Remove(this);
            }
            //GameManager.instance.enemiesInScene.Remove(this.gameObject);
            GameManager.instance.RemoveEnemy(gameObject);
            Destroy(gameObject);
        }
    }


    //public void LookAtSpecial(GameObject target)
    //{
    //    Vector3 looktarget = target.transform.position;
    //    looktarget.y = gameObject.transform.position.y;

    //    //transform.LookAt(looktarget);

    //    transform.LookAt(looktarget);

    //}
}
