using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField]
    private GameObject spawner;
    public GameState gameState;
    private GameObject mainCamera;
    [SerializeField]
    private float enemyCount = 0;
    [SerializeField]
    private float maxEnemies = 5;
    [SerializeField]
    private float minEnemies = 2;

    private bool canSpawn = true;

    public List<GameObject> enemiesInScene = new List<GameObject>();

    //Player
    private GameObject player;
    private PlayerController controls;

    //Player gamestats
    private string runName;
    [SerializeField]
    private int enemiesKilled = 0;
    private float endTime = 0;

    private UIManager UI;

    //Time Variables
    [SerializeField]
    private GameObject timer;
    private float seconds = 0;
    private float minutes = 0;

    [SerializeField]
    private GameObject enemyCounter;

    private float secondsToRemove = 0;

    [SerializeField]
    private GameObject sliderHolder;
    private Slider slider;


    public float incrementTimer;
    public float incrementDelay = 20f;
    public bool flipper;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //References
        player = GameObject.FindGameObjectWithTag("Player");
        controls = player.GetComponent<PlayerController>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        UI = GetComponent<UIManager>();
        slider = sliderHolder.GetComponent<Slider>();
        slider.maxValue = controls.health;

        gameState = GameState.Pregame;
    }

    void FixedUpdate()
    {
        switch (gameState)
        {
            case GameState.Pregame:

                break;
            case GameState.Game:
                GameLoop();
                break;
            case GameState.PostGame:

                break;
        }
        slider.value = controls.health;
        SetTimer();
        enemyCounter.GetComponent<TextMeshProUGUI>().text = ("elims: " + enemiesKilled);

        if (gameState == GameState.Game)
        {
            IncrementorUpdateFunct();
        }
    }

    void IncrementorUpdateFunct()
    {

        if (Time.time > incrementTimer)
        {
            incrementTimer = Time.time + incrementDelay;
            maxEnemies++;
            if (flipper == true)
            {
                flipper = false;
                minEnemies++;
            }
            else
            {
                flipper = true;
            }
        }
    }

    void SetTimer()
    {
        float tempTime = Time.time;

        if (minutes < 1)
            seconds = Mathf.Round(Time.time) - secondsToRemove;
        else
            seconds = Mathf.Round(Time.time) - (minutes * 60) - secondsToRemove;

        if (seconds >= 60 && gameState == GameState.Game) //Minutes incriment every 60 seconds and every 2 minutes a $5000 * minutes payment is due
        {
            minutes++;
        }

        if (seconds < 10) //Set Timer text value to minutes : seconds
            timer.GetComponent<TextMeshProUGUI>().text = minutes.ToString() + ":0" + seconds.ToString();
        else
            timer.GetComponent<TextMeshProUGUI>().text = minutes.ToString() + ":" + seconds.ToString();
    }

    void GameLoop()
    {
        if (enemyCount < maxEnemies && enemyCount >= minEnemies && canSpawn) //Small chance to spawn enemies if count is in range of min/max
        {
            //Chance to spawn enemy
            StartCoroutine(SpawnSpawner(25, 2));
        }
        else if (enemyCount < minEnemies && canSpawn) //Maintain a minimum number of enemies in scene so that there will never be zero
        {
            //Guaranteed enemy spawn
            StartCoroutine(SpawnSpawner(100, 2));
        }
    }

    public void StartGame() //Will start game
    {
        float timeToRemove = Mathf.Round(Time.time);
        secondsToRemove = Mathf.Round(timeToRemove);

        SetTimer();
        gameState = GameState.Game;
    }

    public void PlayerDied()
    {
        endTime = (minutes * 60) + seconds; //Time in seconds
        UI.SetEndScreenStats(enemiesKilled, endTime);

        gameState = GameState.PostGame;
    }

    public void ConfirmScore(string aName)
    {
        runName = aName;

        UI.AddNewScore(runName, enemiesKilled, endTime);

        SceneManager.LoadScene(0);
    }

    IEnumerator SpawnSpawner(float chance, float timer)
    {
        canSpawn = false;

        int rnd = Random.Range(1, 101);

        GameObject newSpawner = spawner;

        if (rnd <= chance)
            Instantiate(newSpawner, NewSpawnerLocation(), Quaternion.identity);

        yield return new WaitForSeconds(timer);
        canSpawn = true;
    }

    public void AddEnemy(GameObject aEnemy)
    {
        enemiesInScene.Add(aEnemy);
        enemyCount++;
    }

    public void RemoveEnemy(GameObject aEnemy)
    {
        foreach (GameObject g in enemiesInScene)
        {
            if (g == aEnemy)
            {
                enemiesInScene.Remove(aEnemy);
                enemiesKilled++;
                enemyCount--;
            }
        }
    }

    Vector3 NewSpawnerLocation()
    {
        float rnd1 = Random.Range(-6f, 6f);
        float rnd2 = Random.Range(-6f, 6f);

        Vector3 newPos = new Vector3(rnd1,1.1f,rnd2);

        return newPos;
    }
}


public enum GameState
{
    Pregame,
    Game,
    PostGame
}