using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHiveMind : MonoBehaviour
{
    public float attackTimer;
    public float attackDelayMin = .5f;
    public float attackDelayMax = 2f;

    public bool TestButton;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TestButton)
        {
            EngageAnEnemy();
            TestButton = false;
        }

        if (GameManager.instance.gameState == GameState.Game)
        {
            if (Time.time > attackTimer)
            {
                float randomDelay = Random.Range(attackDelayMin, attackDelayMax);

                attackTimer = Time.time + randomDelay;
                EngageAnEnemy();
            }
        }
    }


    public void GameUpdateFunct()
    {

    }

    public void EngageAnEnemy()
    {

        int emergencyStopper = 0;
        while (emergencyStopper < 100)
        {
            emergencyStopper++;
            int randomIndex = Random.Range(0, GameManager.instance.enemiesInScene.Count);
            EnemyController enemy = GameManager.instance.enemiesInScene[randomIndex].GetComponent<EnemyController>();
            if (enemy.enemyState != EnemyController.EnemyState.knockedDown && enemy.enemyState != EnemyController.EnemyState.knockedOut)
            {
                enemy.Engage();
                break;
            }

            if (emergencyStopper > 1000)
            {
                break;
                Debug.LogError("EMERGENCYSTOP");
            }
        }
        foreach (GameObject enemy in GameManager.instance.enemiesInScene)
        {
            EnemyController enemyController = enemy.GetComponent<EnemyController>();
            //if ()
        }
    }
}
