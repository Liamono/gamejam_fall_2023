using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DizzyMarker : MonoBehaviour
{
    public float speed = 10f;

    public GameObject target;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime*speed);
        transform.position = target.transform.position + offset;
    }
}
