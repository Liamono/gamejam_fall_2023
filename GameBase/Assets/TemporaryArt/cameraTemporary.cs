using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraTemporary : MonoBehaviour
{
    public GameObject targetObject;
    public float lerpFactor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, targetObject.transform.rotation, Time.deltaTime * lerpFactor);
        transform.position = Vector3.Lerp(transform.position, targetObject.transform.position, Time.deltaTime * lerpFactor);
    }
}
