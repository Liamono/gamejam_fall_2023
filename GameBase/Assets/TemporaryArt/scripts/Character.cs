using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public float health = 3f;
    
    public virtual void TakeDamage(Character dealer, float damage)
    {
        health = health - damage;
    }

    public void LookAtSpecial(GameObject target)
    {
        Vector3 looktarget = target.transform.position;
        looktarget.y = gameObject.transform.position.y;
        transform.LookAt(looktarget);

    }

}
