using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputScript : MonoBehaviour
{
    public Vector2 leftStickInput;
    public Vector2 rightStickInput;
    public bool xButton;
    public bool testButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LeftStick(InputAction.CallbackContext context)
    {
        leftStickInput = context.ReadValue<Vector2>();
    }
    public void RightStick(InputAction.CallbackContext context)
    {
        rightStickInput = context.ReadValue<Vector2>();
    }
    public void XButton(InputAction.CallbackContext context)
    {
        xButton = context.performed;
    }

    public void TButton(InputAction.CallbackContext context)
    {
        testButton = context.performed;
    }
}
