using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemDetector : MonoBehaviour
{
    public PlayerController player;
    public EnemyController currentTarget;
    public GameObject targetIndicator;
    GameObject playerTransform;

    public List<EnemyController> enemiesToAttack = new List<EnemyController>();
    // Start is called before the first frame update
    void Start()
    {
        playerTransform = transform.parent.gameObject;
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        GetNearestValidEnemy();
        debugIndicator();
        centerOnPlayer();
        FaceControlStick();
    }

    bool CheckEnemy(EnemyController enemy, List<EnemyController> enemiesToRemove)
    {
        if (enemy == null)
        {
            enemiesToRemove.Add(enemy);
            return false;
        }
        else
        {
            return true;
        }
    }

    public void GetNearestValidEnemy()
    {
        List<EnemyController> enemiesToRemove = new List<EnemyController>();

        float currentLowest = Mathf.Infinity;
        EnemyController currentNearest = null;

        foreach (EnemyController enemy in enemiesToAttack)
        {
            if (enemy.enemyState != EnemyController.EnemyState.knockedDown && enemy.enemyState != EnemyController.EnemyState.knockedOut)
            {
                float distance = Vector3.Distance(gameObject.transform.position, enemy.transform.position);
                if (distance < currentLowest)
                {
                    currentLowest = distance;
                    currentNearest = enemy;
                }
            }
        }

        if (currentNearest != null)
        {
            currentTarget = currentNearest;

        }
        else
        {
            currentTarget = null;
        }


    }

    void debugIndicator()
    {
        if (currentTarget == null)
        {
            targetIndicator.SetActive(false);
        }
        else
        {
            targetIndicator.SetActive(true);
            targetIndicator.transform.position = currentTarget.transform.position;
        }
    }

    void FaceControlStick()
    {

        if (player.inputScript.leftStickInput.magnitude > .1f)
        {
            float stickAngle = Mathf.Atan2(player.inputScript.leftStickInput.x, player.inputScript.leftStickInput.y);
            stickAngle = stickAngle * Mathf.Rad2Deg;
            Quaternion targetAngle = Quaternion.Euler(0, stickAngle, 0);

            Vector3 lol = transform.position - Camera.main.transform.position;
            Vector3 flattenedLol = lol;
            flattenedLol.y = 0;

            Vector3 controlLol = new Vector3(player.inputScript.leftStickInput.x, 0, player.inputScript.leftStickInput.y);

            Vector3 newLol = flattenedLol;
            newLol = Quaternion.AngleAxis(stickAngle, Vector3.up) * newLol;
            newLol = newLol.normalized;
            float UltraAngle = Mathf.Atan2(newLol.x, newLol.z);
            UltraAngle = UltraAngle * Mathf.Rad2Deg;
            Quaternion newTargetAngle = Quaternion.Euler(0, UltraAngle, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, newTargetAngle, 50 * Time.deltaTime);
            //transform.rotation = newTargetAngle;
        }

    }

    void centerOnPlayer()
    {
        transform.position = playerTransform.transform.position;
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"collision:{collision.collider.gameObject.name}");

    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"triggered:{other.gameObject.name}");
        if (other.gameObject.tag == "Enemy")
        {
            EnemyController enemy = other.GetComponent<EnemyController>();

            if (!enemiesToAttack.Contains(enemy))
            {
                enemiesToAttack.Add(enemy);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            EnemyController enemy = other.GetComponent<EnemyController>();

            if (enemiesToAttack.Contains(enemy))
            {
                enemiesToAttack.Remove(enemy);
            }
        }
    }
}
