using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraTarget : MonoBehaviour
{
    public PlayerController playerTarget;
    public Vector3 offset;
    public float lerpFactor = 5f;
    public float lerpFactorM = 20f;

    public float clampXMin = -30;
    public float clampXMax = 30;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {

        MoveToPlayer();
        if (playerTarget.gameManager.gameState == GameState.Game)
        {
            RespondToRightStick();
        }
        
    }

    public void MoveToPlayer()
    {
        transform.position = Vector3.Lerp(transform.position, playerTarget.gameObject.transform.position + offset, Time.deltaTime * lerpFactorM);
    }

    public void RespondToRightStick()
    {
        Vector2 theVect = playerTarget.inputScript.rightStickInput;
        //float stickAngle = Mathf.Atan2(theVect.x, theVect.y);
        //stickAngle = stickAngle * Mathf.Rad2Deg;
        Vector3 eulerAngle = new Vector3(theVect.y, theVect.x, 0);


        //Quaternion deltaAngle = Quaternion.Euler(theVect.y, theVect.x, 0);
        transform.Rotate(eulerAngle * Time.deltaTime * lerpFactor);
        Vector3 lockableVect = transform.rotation.eulerAngles;
        lockableVect.z = 0;
        if (lockableVect.x > clampXMin)
        {
            lockableVect.x = clampXMin;
        }
        if (lockableVect.x < clampXMax)
        {
            lockableVect.x = clampXMax;

        }

        transform.rotation = Quaternion.Euler(lockableVect);

        //transform.rotation = Quaternion.Lerp(transform.rotation, targetAngle, lerpFactor * Time.deltaTime);
    }
}
