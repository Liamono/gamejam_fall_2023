using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerAttack : MonoBehaviour
{
    public PlayerController player;

    public EnemyController currentTarget;

    public float closeRange = 1f;
    public float midRange = 2f;
    public float farRange = 5f;

    public AttackState attackState;
    public enum AttackState
    {
        none,
        lerping,
        attacking,
    }

    public List<string> closeAttackStates = new List<string>();
    public List<string> midAttackStates = new List<string>();
    public List<string> farAttackStates = new List<string>();

    public GameObject targetMarker;
    public float testDesiredDistance = 1.5f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       // setMarker();
    }

    public void Attack(EnemyController target) 
    {
        currentTarget = target;
        //gameObject.transform.LookAt(currentTarget.transform.position);
        player.LookAtSpecial(target.gameObject);
        attackState = AttackState.attacking;

        float distance = Vector3.Distance(gameObject.transform.position, currentTarget.transform.position);

        if (distance < closeRange)
        {
            string randomState = randomlyChosenState(closeAttackStates);
            Debug.Log($"close attack for state {randomState}");
            player.anim.CrossFade(randomState, .15f);

        }
        else if (distance < midRange)
        {
            string randomState = randomlyChosenState(midAttackStates);
            Debug.Log($"mid attack for state {randomState}");
            player.anim.CrossFade(randomState, .15f);

        }
        else if(distance < farRange)
        {
            string randomState = randomlyChosenState(farAttackStates);
            Debug.Log($"far attack for state {randomState}");
            player.anim.CrossFade(randomState, .15f);

        }
    }

    string randomlyChosenState(List<string> stateList)
    {
        int randomstateIndex = Random.Range(0, stateList.Count);
        return stateList[randomstateIndex];
    }

    void closeAttack()
    {

    }

    void setMarker()
    {
        if (player.enemyDetector.currentTarget != null)
        {
            GameObject theTarg = player.enemyDetector.currentTarget.gameObject;
            Vector3 direction = transform.position - theTarg.transform.position;
            direction = direction.normalized;
            Vector3 desiredPosition = direction * testDesiredDistance + theTarg.transform.position;
            targetMarker.transform.position = desiredPosition;
        }

    }
}
