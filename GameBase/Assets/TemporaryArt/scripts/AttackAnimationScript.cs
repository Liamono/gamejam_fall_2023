using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimationScript : StateMachineBehaviour
{
    public PlayerController player;
    public float IASA = .9f;

    public bool useLerp;
    public float lerpStart = -1;
    public float lerpEnd = -1;
    public float desiredRange = 1f;

    public bool hasAppliedDamage;
    public float hitTime = 2f;
    public float hitDistance = 1.5f;

    public Vector3 startPoint;
    //public Vector3 endPoint;
    public bool hasSetStart;


    public string thuribleAttackState;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.gameObject.GetComponent<PlayerController>();

        player.canAttack = false;
        hasSetStart = false;
        hasAppliedDamage = false;
        animator.applyRootMotion = true;

        if (hitTime < 2f)
        {
            player.thuribleScript.Attack(thuribleAttackState);

        }


        //player.
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player.LookAtSpecial(player.attackScript.currentTarget.gameObject);

        if (stateInfo.normalizedTime > IASA)
        {
            player.canAttack = true;

        }
        else
        {
            player.canAttack = false;
        }

        if (useLerp)
        {
            if (stateInfo.normalizedTime > lerpStart && stateInfo.normalizedTime < lerpEnd)
            {
                animator.applyRootMotion = false;
                if (!hasSetStart)
                {
                    startPoint = player.transform.position;
                    hasSetStart = true;
                }
                Vector3 endPoint = player.transform.position - player.attackScript.currentTarget.transform.position;

                endPoint = endPoint.normalized;
                endPoint = endPoint * desiredRange + player.attackScript.currentTarget.transform.position;
                endPoint.y = player.startHeight.y;

                //calculate the lerp timings
                float lerpDuration = lerpEnd - lerpStart;
                float currentLerpValue = (stateInfo.normalizedTime - lerpStart) / (lerpEnd - lerpStart);

                player.transform.position = Vector3.Lerp(startPoint, endPoint, currentLerpValue);
            }
            else
            {
                animator.applyRootMotion = true;
            }
        }


        if (stateInfo.normalizedTime > hitTime)
        {
            if (hasAppliedDamage == false)
            {
                Debug.Log("Player attempting to hit...");

                hasAppliedDamage = true;
                float distance = Vector3.Distance(player.gameObject.transform.position, player.attackScript.currentTarget.transform.position);
                if (distance < hitDistance)
                {
                    //PlayerController player = enemy.player.GetComponent<PlayerController>();
                    //player.anim.applyRootMotion = false;
                    player.attackScript.currentTarget.TakeDamage(player, 1f);

                    Vector3 theVector = (player.WeaponReference.transform.position + player.attackScript.currentTarget.ragdollManager.testRB.transform.position) / 2;
                    GameObject.Instantiate(player.hitEffectPrefab, theVector, Quaternion.identity);

                    //enemy.player.GetComponent<PlayerController>().LookAtSpecial(enemy.gameObject);
                }

            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        hasAppliedDamage = false;

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
